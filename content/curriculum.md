+++
date = "2015-07-28T09:51:51-04:00"
draft = false
title = "Curriculum"
+++

<h4>Objective Overview</h4>

* Develop great games and simulators. 
* Bring value and good experience to the users.

<h4>Education</h4>

* **Computer Science - UFSC**
* **Degree project title**: Magnetic tracking integration to manual welding simulator with virtual reality using Unity3D
* **Original project title (Portuguese, Brazil)**: Integração de um tracking magnético a um simulador de soldagem manual com realidade virtual utilizando Unity3D

<h4>Experience</h4>

* **[DOT Digital Group](http://dotgroup.com.br/)** *(2015 – Atual)*
* **[Simulógica](http://www.simulogica.com.br/)** *(2013 – 2015)*
* **[nKey Mobile Solutions](https://www.site.nkey.com.br/home-en)** *(2011 – 2012)*
* **[Simulógica](http://www.simulogica.com.br/)** *(2010 – 2011)*
* **[Lapix - Computer Graphics Lab UFSC](http://www.lapix.ufsc.br/)** *(2009 – 2010)*

<h4>Skills</h4>

* Programming Languages
    * C#
    * C/C++
    * Objective-C
    * Javascript
    * PHP 

* API's and SDK's
    * Unity3D 
    * Cocos
    * Box2D

<h4>Personal Projects</h4>

* **Personal Blog**: https://doctorzeroth.wordpress.com/
* **iOS App**: http://rainbug.com.br/floripa-by-music/
* **Personal Game** https://www.youtube.com/watch?v=W0SNOQ_YibQ (Game developed for iOS, video of Gameplay)
* Preparing the others... bestseller, tanks, others unfinished

<h4>Portfolio</h4>

* [Shooting Stand](/post/stand-simulator/)
* [Police Approuch Simulator](/post/stand-simulator/)
* [Candy Run](/post/candy-run)
* [Paper and Arrow](/post/paper=and-arrow)
* [Floripa by Music](/post/floripa-by-music)
* [Welding Simulator](/post/welding-simulator/)
* [ABF Franchises Training](/post/abf/)
* [Pixeon Showcase](/post/pixeon/)
* [Button Football](/post/football-table/)
* [AR](/post/augmented-reallity)
* [Bestseller ranking](/post/bestsellerranking)
* [Decision Tree Framework](/post/decision-tree-framework/)
* [Training with Interative Story](/post/petroleo/)
* [Corporative Quiz Game](/post/questions-game/)
* [CPR](/post/cpr-simulator/)
* [Stand as Entertainment](/post/stand-extension/)

<h4>Activity</h4>

* Extra Courses
    * Game development in Java (2007) SENAI Tubarão - SC (Brazil)
        * Game engine, colisions, maps, game loop, timer, input treatment, drawing, simulation, audio, sprites and animation, video buffer
    * 3D max Fundamental (2005) AZIMUT - RJ
        * Cameras and light, mapping, modifyers, splines, poly structure, animation, mental ray, material editor, particles
    * Ilumination Technique
        * Internal and External ilumination, radiosity, shadow types, projections and volumetric effects, light control and colors, light positioning, reflection, refraction, ambient light
    * Character Studio (2006) Autodesk - ATC
        * Rig, apply Physique, create, edit and generate walking footsteps, animate with animation key, combine animations to create a sequence, IK linking, flowk animation, bones

* Events
    * XI Simpósio Brasileiro de jogos e entretenimento digital (SBGames)
    * IX Simpósio Brasileiro de jogos e entretenimento digital (SBGames)
    * 11º Fórum Internacional de Software Livre (FISL)
    * 10º Fórum Internacional de Software Livre (FISL)
    * Unite Brasil, 2013

<h4>Contact</h4>

* [email](mailto:matheus.tf.zero@gmail.com?Subject=Contact)
* [linkedin](https://br.linkedin.com/in/matheus-fernandes-77997924)