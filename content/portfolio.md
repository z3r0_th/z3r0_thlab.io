+++
date = "2015-07-28T09:51:51-04:00"
draft = false
title = "Portfolio"

[[featured]]
url = "/post/stand-simulator/"
image = "/simulogica/feature_stand2.jpg"
caption = "Stand Simulator"

[[featured]]
url = "/post/stand-simulator/"
image = "/simulogica/feature_1802.jpg"
caption = "Approuch Simulator"

[[featured]]
url = "/post/welding-simulator/"
image = "/simulogica/solda/feature_welding2.jpg"
caption = "Welding Simulator"

[[featured]]
url = "/post/candy-run/"
image = "/nkey/candyrun/feature_candyrun2.jpg"
caption = "Candy Run"

[[featured]]
url = "/post/abf/"
image = "/dot/abf/feature_abf2.jpg"
caption = "Franchise Training"

[[featured]]
url = "/post/pixeon/"
image = "/dot/pixeon/feature_pixeon2.jpg"
caption = "Pixeon Showcase"

[[featured]]
url = "/post/football-table/"
image = "/dot/soccer/feature_football2.jpg"
caption = "Button Football"

[[featured]]
url = "/post/questions-game/"
image = "/dot/quiz/feature_quiz.jpg"
caption = "Corporate Quiz"

[[featured]]
url = "/post/augmented-reallity/"
image = "/dot/ddr/feature_ar.jpg"
caption = "Realidade Aumentada"

[[featured]]
url = "/post/floripa-by-music/"
image = "/personal/floripa-by-music/feature_floripa.jpg"
caption = "Floripa by Music"

[[featured]]
url = "/post/bestsellerranking/"
image = "/personal/bestsellerranking/feature_bestseller.jpg"
caption = "Best seller ranking"

[[featured]]
url = "/post/paper-and-arrow/"
image = "/personal/paper-and-arrow/feature_paper.jpg"
caption = "Paper and arrow"

[[featured]]
url = "/post/decision-tree-framework/"
image = "/dot/decision-tree/feature_tree.jpg"
caption = "Decision Tree Framework"

[[featured]]
url = "/post/petroleo/"
image = "/dot/petro/feature_petro.jpg"
caption = "Training with Interative Story"

[[featured]]
url = "/post/cpr-simulator/"
image = "/dot/cpr/feature_cpr.jpg"
caption = "CPR Simulator"

[[featured]]
url = "/post/stand-extension/"
image = "/mediasoft/feature_range.jpg"
caption = "Stand as Entertainment"
+++


