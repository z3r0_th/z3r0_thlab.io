+++
date = "2015-08-15T09:32:45-04:00"
draft = false
title = "ABF - franchises training"
tags = ["educational", "training", "cocos", "cocos-js", "dotGroup", "mobile"]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/)/[**ABF/SEBRAE**](http://www.abf.com.br/abf-e-sebrae-inovam-ao-lancar-game-educacional-na-abf-expo-2015/) </br>

***Product***: This is an educational game.The  intent is to teach about busines management, stock control, marketing, staff training, etc. 
The main focus is on franchises. </br>

***My Participation***: I entered later on the development, about 75% of the project was already developed. I had to finish and publish it. 
The mobile publication was a challenge, as we had some trouble because some conflicts due to the way it was initially writen. After debugging 
we found out some sintaxe detail that was not supported in mobile deploy.</br>

***Learning***: I already knew cocos2d, but it was a new, but yet familiar, experience to develop using cocos-js. 
It was an interersting experience to take someone elses code and continue with development; I had no trouble to do so. 
I learned to deploy and use android tools too. </br>

![In Game Picture](/dot/abf/capturar4.png#centered)

![Goals](/dot/abf/capturar3.png#centered)

![Main Menu](/dot/abf/capturar.png#centered)
