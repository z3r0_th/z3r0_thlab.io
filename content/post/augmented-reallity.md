+++
date = "2017-02-04T09:32:45-04:00"
draft = false
title = "Augmented Reallity"
tags = ["AR", "DOTGroup", "augmented reallity", "unity3D"]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: [This project](https://play.google.com/store/apps/details?id=com.DOT.DDRApp) is an agmented reallity catalog, it servers as a tool for company's sellers
and a product example. </br>

***My Participation***: In this project I worked as consultant, helping in the development, but not coding it self.
I'm working in other AR products though, but it is not public yet. I will update when it is ready. </br>


![Scene](/dot/ddr/captura5.jpg#centered)
