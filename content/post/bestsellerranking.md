+++
date = "2016-11-16T09:32:45-04:00"
draft = false
title = "Bestseller Ranking"
tags = ["php", "site", "personal"]
+++


***Company***: **My Self** </br>

***Product***: This website is a service for the companies. The ideia is to anonimously share information.
For example, to get information one shares its revenues and receive the average revenue nearby. </br>

***My Participation***: I developed the back-end (php and mysql) and helped to develop the front-end (mainly angular requests and presentation) </br>

***Learning***: Learned php and angular, that I have never used before </br>



![Screensshot](/personal/bestsellerranking/screenshot.png#centered)
