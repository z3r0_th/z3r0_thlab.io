+++
date = "2012-07-20T09:32:45-04:00"
draft = false
title = "Candy Run"
tags = ["nkey", "mobile",  "iOS", "cocos", "cocos2D", "box2d"]
+++


***Company***: [**nKey**](https://www.site.nkey.com.br/portfolio) </br>

***Product***: This is an iOS exclusive native game. It was developed using XCode and objective-c with Cocos2D and box2D. 
It is a simple endless running game.  </br>

***My Participation***: nKey was experimenting with games at the time and it was the first game to experiment with. I handled
most of the programming stuff. We used native code for iOS (main focus of the company at the time), using cocos2D and box2D.

For me, the most difficult and intersting part to work on was the "water" inside the thermometer - It shaked as it got heigher or lower.</br>

***Learning***: I learned cocos2D and box2D. I had very low experience with theses two technologies at the time.</br>

![Game](/nkey/candyrun/Ascreen480x480.jpeg#centered)

![Store](/nkey/candyrun/Bscreen480x480.jpeg#centered)

![Menu](/nkey/candyrun/Cscreen480x480.jpeg#centered)
