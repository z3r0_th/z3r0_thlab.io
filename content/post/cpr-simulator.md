+++
date = "2017-02-28T09:32:45-04:00"
draft = false
title = "Cardiopulmonary resuscitation"
tags = ["dotGroup", "training", "unity3D", "game",]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: This simulator will be part of bigger medicinal course. The ideia is to refforce the studies and help student to
memorize the correct protocol. </br>

***My Participation***: I programmed the game in Unity3D. </br>

***Learning***: The biggest challenge, and what i learned the most is to optmize the game to run well in webgl and mobile. </br>


![Screenshot](/dot/cpr/small_capturar4.jpg#centered)

![Screenshot](/dot/cpr/small_capturar2.jpg#centered)
