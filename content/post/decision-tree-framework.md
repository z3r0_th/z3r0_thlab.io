+++
date = "2016-02-28T09:32:45-04:00"
draft = false
title = "Decision Tree Framework"
tags = ["framework", "decisionTree", "state machine", "dotGroup"]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

*Product*: This is an internal framework to easily make interactive stories.
As we saw the demand for interactive stories games rise, we decided to make a framework 
to help Game Designers to quickly build something interesting. 

Today, some games can be build without the programmers, lowering the prices and the time to delivery.
 </br>

***My Participation***: I had the oportunity to build the whole framework. I developed it in cocos2d-js. 
But it is based on JSON, so it is very easy to port. To help even more our game designer, I developed 
an excel parser to parse the sheet the game designer was confortable to our framework. </br>

***Learning***: In this project I was already very confortable with the technologies. 
I got some experience on how to develop a framework to be reusable.</br>


[![decision-tree](/dot/decision-tree/small_tree.jpg#centered)](/dot/decision-tree/tree.jpg)
