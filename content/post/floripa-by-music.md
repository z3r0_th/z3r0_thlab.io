+++
date = "2014-11-24T09:32:45-04:00"
draft = false
title = "Floripa by Music"
tags = ["personal", "iOS", "app"]
+++


***Company***: **My Self and [Moyses](https://www.linkedin.com/in/moysesd/)** </br>

***Product***: [Floripa by Music](https://www.facebook.com/FloripaByMusic/) is an app about knowing 
the city (Florianópolis - SC) by it's music. Each invited artist can select a city's spot, say something about it
select it's own featured music that will be avaliable to the user to listen. 

The user can, then, know the city by the local artist view and, using GPS, listen to the selected music on the spot.</br>

***My Participation***: I developed the entire app using XCode and objective-c while [Moyses](https://www.linkedin.com/in/moysesd/) 
handled the artist negotiation, marketing etc.</br>

***Learning***: After a while without programming in objective-c, I had to remember it. It wasn't difficult, 
but somethings had changed. 

I have learned to plan first as well. We got wrong with our user base. In Brazil the biggest user base is Android. 
We choosed iphone as it would be easier to develop, because I had a background and this way could finish it faster. 
Indeed, we finished faster, but it was a mistake. We should have planned to Android first and think in the end user first. </br>

![Screen1](/personal/floripa-by-music/map.jpg#centered)

![Screen2](/personal/floripa-by-music/screen1.jpg#centered)

![Screen3](/personal/floripa-by-music/screen2.jpg#centered)

<iframe width="560" height="315" src="https://www.youtube.com/embed/kDBHO5bWFgw" frameborder="0" allowfullscreen></iframe>
