+++
date = "2016-07-12T09:32:45-04:00"
draft = false
title = "Button Football"
tags = ["dotGroup", "table", "unity3D"]
+++

***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: This was an experiment to test a multtouch "table" (a screen of 55") . It was an ideia of myself and developed with help of my colegue and friend Sander Nonaka, 
a great artist. We used a week spared by the company to develop this internal experiment. 

This game is a "button football". You use your finger to shoot the players, hit the ball and score </br>

***My Participation***: I developed the whole game, using Unity3D, in a week and half. </br>

***Learning***: I got more experienced in develop to multtouch devices.  </br>


![VS](/dot/soccer/vs.png#centered)

![game-play](/dot/soccer/playing2.png#centered)

![selecting-tatics](/dot/soccer/taticsandteam.png#centered)
