+++
date = "2016-03-28T09:32:45-04:00"
draft = false
title = "Oil Company Leader Training with Interative Story"
tags = ["oil", "DOTGroup", "decisionTree", "cocos", "cocos-js", "training"]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: An interactive story game with many branchs and decisions to make. It was developed using the 
[decision tree framework]("/post/decision-tree-framework/"). The was a demand of a big brasilian oil company to leader training.</br>

***My Participation***: I developed the game using cocos-js and the decision tree framework developed previously.</br>

***Learning***: I already was confortable with the techlogy. I had to develop with close deadline and constant change demands from the client. </br>

[![Screenshot](/dot/petro/small_capturar6.jpg#centered)](/dot/petro/capturar6.png)

[![Screenshot](/dot/petro/small_capturar2.jpg#centered)](/dot/petro/capturar2.png)

[![Screenshot](/dot/petro/small_capturar.jpg#centered)](/dot/petro/capturar.png)
