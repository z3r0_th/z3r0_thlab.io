+++
date = "2015-07-28T09:32:45-04:00"
draft = false
title = "Pixeon"
tags = ["VR", "virtual reallity", "dotGroup", "unity3D"]
+++


***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: This project was devoloped as a showcase of pixeon's future vision. It was featured in 
[**Hospitalar 2016**](http://pixeon.com.br/noticias/3707/futuro-da-health-it-pixeon-leva-ambiente-de-realidade-virtual-para-hospitalar-2016/).

It uses Occulus Rift and [Leap Motion](https://doctorzeroth.wordpress.com/2016/01/27/leapmotion-intro/) to bring the user in an imersive experience, where the user can interact with the enviroment, 
pick and handle virtual x-rays with it's own hands.</br>

***My Participation***: The manager brought the challenge to my self and an artist (Sander Nonaka) develop this application within one mounth. 
Challenge accepted and gracefully executed. We managed to develop one full tutorial and the main application; recognizing gestures as point to select,
thumbs up to ok, scale up and down, pinch to hold and more. Everything, from concept to the finished Product was developed within the time-line of one mounth.</br>

***Learning***: With this project I got more experience with VR and learned a lot about leap motion.</br>

![InGame](/dot/pixeon/capturar1.png#centered)

![Contrast](/dot/pixeon/capturar4.png#centered)
