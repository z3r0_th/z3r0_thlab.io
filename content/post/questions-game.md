+++
date = "2016-09-09T09:32:45-04:00"
draft = false
title = "Corporative Quiz Game"
tags = ["dotGroup", "training", "unity3D", "game", "quiz"]
+++

***Company***: [**DOT Group**](http://dotgroup.com.br/) </br>

***Product***: This is a game similar to the popular [songpop](https://apps.facebook.com/songpop/) 
but with question and answers, as a quiz, instead of song and artsits. The goal is to help the staff training.
It features a server with an admin site and a multiplatform game. User can challange other users within the same Group 
to see who answers faster the questions. </br>

***My Participation***: I did not programmed server nor the front-end, 
but I did have to maintain and change some features. It was developed in node.js and angular. 

I developed the whole game using Unity3D. </br>

***Learning***: I have no experience with node.js or angular. So maintain the server and site was an exciting challenge.

I got more experience with Unity3D and learned some new features and used some third assets. 

In this case we have learned that this kind of corporation game customization is very important. </br>

![Screen1](/dot/quiz/small_capturar10.jpg#centered)

![Screen2](/dot/quiz/small_capturar6.jpg#centered)

![Screen3](/dot/quiz/small_capturar9.jpg#centered)

