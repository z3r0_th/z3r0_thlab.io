+++
date = "2017-04-28T09:32:45-04:00"
draft = false
title = "Stand as Entertainment"
tags = ["mediasoft", "stand", "unity3D", "training", "entertainment"]
+++


***Company***: **Mediasoft** </br>

***Product***: The stand simulator is a simulator and do not have space to entertainment applications. But there's such space in the market.
Using the simulator core, we developed a spin off for entertainment.  </br>

***My Participation***: I have lots of experience with the stand simulator, so I was hird to do the whole spin off. </br>

***Learning***: I focused to learn how to let the game "modable". Enabling us to modify the game without to recompile the entire code. </br>


![Screenshot](/mediasoft/vs_range.png#centered)

