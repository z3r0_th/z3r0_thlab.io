+++
date = "2010-02-28T09:32:45-04:00"
draft = false
title = "Stand and Police approach trainning simulator"
tags = ["simulogica", "mediasoft", "stand", "unity3D", "training"]
+++


***Company***: [**Simulógica**](http://www.simulogica.com.br/) </br>

***Product***: This is a shooting stand simulator and police force situational simulator. 
Both has it's software and hardware part. It uses a projector, an infra-red laser adpted 
into the real gun (with optional recoil apdapter), a cam to capture the infra-red. 
At software part, there is a callibrator to make a raw laser adjustment (cam adjustment); 
the stand or the situational simulator it self with a fine adjustment to the laser (gun adjustment). 

The goal is to train police or private secure force. It collects statistics, and is supposed to 
have a coach to guide whos trainning. In the stand, coach is supposed to help with posture, 
with tips to accomplish the games and to make sure the student evolution. In the situational simulator, 
the coach should choose the level, the path and suspects behaviour according to the student reactions. 

Stand features nine trainning modules. From free shot to random double tap game. 
It has the controller and the stand, wich are in constant communication.

Situational simulator features the simulator and an editor. In the editor you can build a path 
for specific level, positioning the suspects and a set of behaviours.
</br>

***My Participation***: Stand and Situational Simulator for police trainning were 
the first projects that I had the opportunity to work on professionaly. 
Before that I worked in Lapix giving support to some image treatment software to medicinal field. 

I Started supporting an old version, already in production at the time, developed in C++ 
using [OGRE](http://www.ogre3d.org/). After months working with the software, we have learned 
that we needed a better and full engine to make faster interactions. I Suggested Unity3D 
(version 3 at time) and helped with the transition. It worked quite well, giving more speed 
and better graphics just as we needed. Until today the basic code and project is the one that I 
helped to design and develop. </br>

***Learning***: I learned a little bit with each module:

* **Stand**; It was the project that made we realize that a fast interaction to deliver better software 
to client in less time and make modifications as needed and demanded very quick was key to a successful product. 
It was my first contact with C# and Unity3D. So it helped me to understand the technology as well.

* **Situational Simulator**; We needed to integrate two applications - One controller (wich controls the simulator)
for the instructor and one simulator to student. I have learned a lot of Unity3D and C# with this project too. 

* **Situational Editor**; I have learned the beneficial use of design patterns. I already nkew it, of course, but here 
we put it in heavily in practice. We could easily support undo because of it.


[![Stand](/simulogica/small_stand.jpg#centered)](/simulogica/stand.jpg)

[![180](/simulogica/small_180.jpg#centered)](/simulogica/180.jpg)
