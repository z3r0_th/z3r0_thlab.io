+++
date = "2016-11-11T09:32:45-04:00"
draft = false
title = "Welding Simulator"
tags = ["simulogica", "unity3D", "welding", "VR", "virtual reality", "training"]
+++

***Company***: [**Simulógica**](http://www.simulogica.com.br/) </br>

***Product***: This is a welding simulator. The main goal is to let the welding student to practice 
in a safe ambient with low cost before it goes to a real situation. This is not a finished product, 
but it can simulate some basic welding already.

It uses VR ([virtual reality](tags/training/VR)) with occulus rift and a magnetic tracking to track the 
welding pistol. </br>

***My Participation***: I used this project as subject to my graduation project: 
"Magnetic tracking integration to manual welding simulator with virtual reality using Unity3D" 
("[Integração de um tracking magnético a um simulador de soldagem manual com realidade virtual utilizando Unity3D](https://repositorio.ufsc.br/handle/123456789/171292)") 

My participation in the project comes from the very begging, helping to develop the tracker integration, 
the visual effects, user interface, collect use information and user error feedback (helping to keep user in the right track).
</br>

***Learning***: With this project I have learned to make some hardware-software integration and to proper use virtual reality  </br>

[![welding](/simulogica/solda/small_capturar.jpg#centered)](/simulogica/solda/capturar.png)

[![welding](/simulogica/solda/small_capturar3.jpg#centered)](/simulogica/solda/capturar3.png)

